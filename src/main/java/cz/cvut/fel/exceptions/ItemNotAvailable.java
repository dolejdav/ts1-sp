package cz.cvut.fel.exceptions;

public class ItemNotAvailable extends Exception {

    public ItemNotAvailable(String title) {
        super(title + " is not available");
    }

}
