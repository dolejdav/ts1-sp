package cz.cvut.fel.enums;

public enum Sorting {
    RELEASE_DATE("release_date_desc"),
    POPULARITY("popularity_desc"),
    PRICE_ASC("type_price_asc"),
    PRICE_DESC("type_price_desc"),
    ALPHABETICAL("title_asc");

    private final String value;

    Sorting(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
