package cz.cvut.fel.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class Utils {

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace(System.err);
        }
    }

    public static void dismissCookieBanner(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.of(5, ChronoUnit.SECONDS));
        try {
            By cookieConsentSelector = By.className("js-cookie-acceptance");

            WebElement cookieConsentButton = wait.until(ExpectedConditions.elementToBeClickable(cookieConsentSelector));

            cookieConsentButton.click();

            System.out.println("Cookie consent banner was present and has been dismissed.");

        } catch (NoSuchElementException | TimeoutException e) {
            System.out.println("Cookie consent banner not present or not clickable at this time.");
        }

    }

}
