package cz.cvut.fel;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class App {
    private WebDriver driver;

    public void start() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.get("https://www.obchod.crew.cz/");

    }

    public void destroy() {
        driver.quit();
    }

    public WebDriver getDriver() {
        return driver;
    }

}
