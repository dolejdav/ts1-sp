package cz.cvut.fel.models;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SearchItem {
    private final WebElement element;
    private String title;
    private boolean available;
    private int price;

    public SearchItem(WebElement element) {
        this.element = element;

        try {
            title = element.findElement(By.className("item__tit")).getText();
        } catch (NoSuchElementException e) {
            title = "";
        }

        available = false;
        List<WebElement> elements = element.findElements(By.tagName("strong"));
        WebElement availabilityElement = elements.stream()
                .filter(e -> e.getAttribute("itemprop") != null && e.getAttribute("itemprop").equals("availability"))
                .findFirst()
                .orElse(null);
        available = availabilityElement != null && availabilityElement.getText().contains("Skladem");

        elements = element.findElements(By.tagName("span"));
        WebElement priceElement = elements.stream()
                .filter(e -> e.getAttribute("itemprop") != null && e.getAttribute("itemprop").equals("price"))
                .findFirst()
                .orElse(null);
        if (priceElement != null) {
            try {
                price = Integer.parseInt(priceElement.getText());
            } catch (NumberFormatException e) {
                price = -1;
            }
        } else {
            price = -1;
        }
    }

    public WebElement getElement() {
        return element;
    }

    public String getTitle() {
        return title;
    }

    public boolean isAvailable() {
        return available;
    }

    public int getPrice() {
        return price;
    }
}
