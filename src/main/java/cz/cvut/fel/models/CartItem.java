package cz.cvut.fel.models;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class CartItem {
    private final WebElement element;
    private String title;
    private int count;

    public CartItem(WebElement element) {
        this.element = element;


        try {
            title = element.findElement(By.className("checkout__tit")).getText();
        } catch (NoSuchElementException e) {
            title = "";
        }

        try {
            WebElement countElement = element.findElement(By.className("quantity-counter"));
            count = Integer.parseInt(countElement.getAttribute("value"));
        } catch (NoSuchElementException e) {
            count = -1;
        } catch (NumberFormatException e) {
            count = 0;
        }
    }

    public WebElement getElement() {
        return element;
    }

    public String getTitle() {
        return title;
    }

    public int getCount() {
        return count;
    }
}
