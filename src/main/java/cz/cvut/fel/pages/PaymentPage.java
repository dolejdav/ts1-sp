package cz.cvut.fel.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class PaymentPage extends Page {

    public PaymentPage(WebDriver driver) {
        super(driver, new WebDriverWait(driver, Duration.of(5, ChronoUnit.SECONDS)));
    }

    public void selectShippingMethod() {
        WebElement element = driver.findElement(By.xpath("/html/body/div[2]/section/div[2]/form/div[1]/div[1]/div[2]/div[6]/label"));
        element.click();
    }

    public void selectPaymentMethod() {
        WebElement element = driver.findElement(By.xpath("/html/body/div[2]/section/div[2]/form/div[1]/div[1]/div[4]/div[6]/label"));
        element.click();
    }

    public void clickNextButton() {
        WebElement button = driver.findElement(By.xpath("/html/body/div[2]/section/div[2]/form/div[2]/div[1]/p/button"));
        button.click();
    }
}
