package cz.cvut.fel.pages;

import cz.cvut.fel.utils.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class LoginPage extends Page {
    @FindBy(id = "login")
    private WebElement loginButton;

    public LoginPage(WebDriver driver) {
        super(driver, new WebDriverWait(driver, Duration.of(5, ChronoUnit.SECONDS)));
    }

    public void login(String email, String password) {
        if(isLogged()) return;

        loginButton.click();

        Utils.sleep(1000);

        WebElement emailInput = driver.findElement(By.id("email"));
        emailInput.sendKeys(email);
        WebElement passwordInput = driver.findElement(By.id("heslo"));
        passwordInput.sendKeys(password + Keys.ENTER);
    }

    public boolean isLogged() {
        List<WebElement> elementList = driver.findElements(By.className("logged"));
        return !elementList.isEmpty() && elementList.get(0).isDisplayed();
    }

}
