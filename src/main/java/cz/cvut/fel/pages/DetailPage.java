package cz.cvut.fel.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class DetailPage extends Page {
    @FindBy(tagName = "h1")
    private WebElement titleElement;

    @FindBy(id = "detail_desc")
    private WebElement descriptionElement;

    public DetailPage(WebDriver driver) {
        super(driver, new WebDriverWait(driver, Duration.of(5, ChronoUnit.SECONDS)));
    }

    public String getTitle() {
        return titleElement.getText();
    }

    public String getDescription() {
        return descriptionElement.getText();
    }

}
