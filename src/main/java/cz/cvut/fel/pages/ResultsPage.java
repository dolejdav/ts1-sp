package cz.cvut.fel.pages;

import cz.cvut.fel.exceptions.ItemNotAvailable;
import cz.cvut.fel.models.SearchItem;
import cz.cvut.fel.utils.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ResultsPage extends Page {
    @FindBy(className = "item")
    private List<WebElement> itemElements;

    private final List<SearchItem> searchItems;

    public ResultsPage(WebDriver driver) {
        super(driver, new WebDriverWait(driver, Duration.of(5, ChronoUnit.SECONDS)));
        searchItems = itemElements.stream()
                .map(SearchItem::new)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public List<SearchItem> getItems() {
        return searchItems;
    }

    public void addToCart(SearchItem item) throws ItemNotAvailable {
        if(!item.isAvailable()) {
            throw new ItemNotAvailable(item.getTitle());
        }

        WebElement addToCart = item.getElement().findElement(By.className("add-to-cart"));
        addToCart.click();

        Utils.sleep(1000);

        WebElement modalCloseButton = driver.findElement(By.className("close"));
        modalCloseButton.click();
    }

    public void goToDetails(SearchItem item) {
        item.getElement().click();
    }
}
