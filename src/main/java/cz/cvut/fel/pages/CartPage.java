package cz.cvut.fel.pages;

import cz.cvut.fel.models.CartItem;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CartPage extends Page {
    @FindBy(className = "js--cart-item")
    private List<WebElement> itemElements;

    private List<CartItem> cartItems;

    public CartPage(WebDriver driver) {
        super(driver, new WebDriverWait(driver, Duration.of(5, ChronoUnit.SECONDS)));
        cartItems = itemElements.stream()
                .map(CartItem::new)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public void removeItem(CartItem cartItem) {
        WebElement removeButton = cartItem.getElement().findElement(By.className("remove-from-cart"));
        removeButton.click();
    }

    public void clickContinueButton() {
        WebElement continueButton = driver.findElement(By.className("js--continue-button"));
        continueButton.click();
    }

    public void refresh() {
        itemElements = driver.findElements(By.className("js--cart-item"));
        cartItems = itemElements.stream()
                .map(CartItem::new)
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
