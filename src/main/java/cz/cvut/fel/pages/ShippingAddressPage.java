package cz.cvut.fel.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class ShippingAddressPage extends Page {
    @FindBy(name = "shipping_name")
    private WebElement shippingNameInput;

    @FindBy(name = "shipping_surname")
    private WebElement shippingSurnameInput;

    @FindBy(name = "email_without_registration")
    private WebElement emailWithoutRegistrationInput;

    @FindBy(name = "shipping_phone")
    private WebElement shippingPhoneInput;

    @FindBy(name = "shipping_street")
    private WebElement shippingStreetInput;

    @FindBy(name = "shipping_city")
    private WebElement shippingCityInput;

    @FindBy(name = "shipping_zip")
    private WebElement shippingZipInput;

    public ShippingAddressPage(WebDriver driver) {
        super(driver, new WebDriverWait(driver, Duration.of(5, ChronoUnit.SECONDS)));
    }

    public void fillName(String shippingName) {
        this.shippingNameInput.sendKeys(shippingName);
    }

    public void fillSurname(String shippingSurname) {
        this.shippingSurnameInput.sendKeys(shippingSurname);
    }

    public void fillEmail(String emailWithoutRegistration) {
        this.emailWithoutRegistrationInput.sendKeys(emailWithoutRegistration);
    }

    public void fillPhone(String shippingPhone) {
        this.shippingPhoneInput.sendKeys(shippingPhone);
    }

    public void fillStreet(String shippingStreet) {
        this.shippingStreetInput.sendKeys(shippingStreet);
    }

    public void fillCity(String shippingCity) {
        this.shippingCityInput.sendKeys(shippingCity);
    }

    public void fillZip(String shippingZip) {
        this.shippingZipInput.sendKeys(shippingZip);
    }

    public void clickNextButton() {
        WebElement button = driver.findElement(By.xpath("/html/body/div[2]/section/div[2]/form/div[2]/div[1]/p/button"));
        button.click();
    }
}
