package cz.cvut.fel.pages;

import cz.cvut.fel.enums.Sorting;
import cz.cvut.fel.utils.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class FiltersPage extends Page {
    @FindBy(name = "sorting")
    private WebElement sortingSelect;

    @FindBy(id = "news")
    private WebElement newsCheckbox;

    @FindBy(id = "action")
    private WebElement actionCheckbox;

    @FindBy(id = "storage")
    private WebElement storageCheckbox;

    @FindBy(id = "shipping")
    private WebElement shippingCheckbox;

    @FindBy(xpath = "/html/body/div[2]/section/div[2]/div/div/form/div[1]/div")
    private WebElement authorInputWrapper;

    public FiltersPage(WebDriver driver) {
        super(driver, new WebDriverWait(driver, Duration.of(5, ChronoUnit.SECONDS)));
    }

    public void searchByAuthor(String author) {
        authorInputWrapper.click();
        Utils.sleep(1000);
        WebElement authorInput = driver.findElement(By.xpath("/html/body/span/span/span[1]/input"));
        authorInput.sendKeys(author);
        Utils.sleep(1000);
        authorInput.sendKeys(Keys.ENTER);
    }

    public void selectSorting(Sorting sorting) {
        String value = sorting.getValue();
        new Select(sortingSelect).selectByValue(value);
    }

    public boolean isNewsChecked() {
        return newsCheckbox.isSelected();
    }

    public boolean isActionChecked() {
        return actionCheckbox.isSelected();
    }

    public boolean isStorageChecked() {
        return storageCheckbox.isSelected();
    }

    public boolean isShippingChecked() {
        return shippingCheckbox.isSelected();
    }

    public void checkNews() {
        WebElement element = driver.findElement(By.className("news"));
        element.click();
    }

    public void checkAction() {
        WebElement element = driver.findElement(By.className("action1"));
        element.click();
    }

    public void checkStorage() {
        WebElement element = driver.findElement(By.className("storage"));
        element.click();
    }

    public void checkShipping() {
        WebElement element = driver.findElement(By.className("shipping"));
        element.click();
    }
}
