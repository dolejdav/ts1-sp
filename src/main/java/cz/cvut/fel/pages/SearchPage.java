package cz.cvut.fel.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class SearchPage extends Page {
    @FindBy(id = "search")
    private WebElement searchBox;

    @FindBy(className = "navbar-crew__basket")
    private WebElement basketButton;

    public SearchPage(WebDriver driver) {
        super(driver, new WebDriverWait(driver, Duration.of(5, ChronoUnit.SECONDS)));
    }

    public void search(String text) {
        searchBox.sendKeys(text);
        searchBox.sendKeys(Keys.ENTER);
    }

    public void clickBasketButton() {
        basketButton.click();
    }
}
