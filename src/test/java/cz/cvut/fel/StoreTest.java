package cz.cvut.fel;

import cz.cvut.fel.enums.Sorting;
import cz.cvut.fel.exceptions.ItemNotAvailable;
import cz.cvut.fel.models.CartItem;
import cz.cvut.fel.models.SearchItem;
import cz.cvut.fel.pages.*;
import cz.cvut.fel.utils.Utils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class StoreTest {

    private final App app;

    public StoreTest() {
        app = new App();
    }

    @BeforeEach
    public void setUp() {
        app.start();

        Utils.sleep(1000);
        Utils.dismissCookieBanner(app.getDriver());
    }

    @AfterEach
    public void tearDown() {
        app.destroy();
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/parametersForComicsSearch.csv", numLinesToSkip = 1)
    public void testSearchComics(String searchTerm, String firstItemTitle) {
        try {
            SearchPage searchPage = new SearchPage(app.getDriver());
            searchPage.search(searchTerm);

            Utils.sleep(2500);

            ResultsPage resultsPage = new ResultsPage(app.getDriver());
            List<SearchItem> results = resultsPage.getItems();

            if (!results.isEmpty()) {
                assertEquals(firstItemTitle, results.get(0).getTitle());
            } else {
                assertEquals(firstItemTitle, "No results found");
            }
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testFilters() {
        try {
            FiltersPage filtersPage = new FiltersPage(app.getDriver());

            filtersPage.selectSorting(Sorting.PRICE_DESC);
            Utils.sleep(2500);

            filtersPage.selectSorting(Sorting.POPULARITY);
            Utils.sleep(2500);

            filtersPage.checkAction();
            Utils.sleep(2500);

            filtersPage.checkStorage();

            Utils.sleep(2500);

            ResultsPage resultsPage = new ResultsPage(app.getDriver());
            List<SearchItem> searchItems = resultsPage.getItems();

            assertEquals("One Piece 1: Romance Dawn - Dobrodružství začíná", searchItems.get(0).getTitle());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/parametersForDetails.csv", numLinesToSkip = 1)
    public void testDetailPage(String searchTerm, String expectedDescription) {
        try {
            SearchPage searchPage = new SearchPage(app.getDriver());
            searchPage.search(searchTerm);

            Utils.sleep(2500);

            ResultsPage resultsPage = new ResultsPage(app.getDriver());
            List<SearchItem> results = resultsPage.getItems();
            assertFalse(results.isEmpty());

            resultsPage.goToDetails(results.get(0));

            DetailPage detailPage = new DetailPage(app.getDriver());

            String description = detailPage.getDescription();

            assertEquals(expectedDescription, description);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testMultipleSearches() {
        try {
            String[] searchTerms = {"Batman", "Superman", "InvalidTerm"};
            SearchPage searchPage = new SearchPage(app.getDriver());
            for (String searchTerm : searchTerms) {
                searchPage.search(searchTerm);

                Utils.sleep(2500);

                ResultsPage resultsPage = new ResultsPage(app.getDriver());
                List<SearchItem> results = resultsPage.getItems();
                if ("InvalidTerm".equals(searchTerm)) {
                    assertTrue(results.isEmpty());
                } else {
                    assertFalse(results.isEmpty());
                }
                Utils.sleep(1000); // wait before next search
            }
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testFilterByNews() {
        try {
            FiltersPage filtersPage = new FiltersPage(app.getDriver());
            filtersPage.checkNews();

            Utils.sleep(2500);

            ResultsPage resultsPage = new ResultsPage(app.getDriver());
            List<SearchItem> searchItems = resultsPage.getItems();
            assertFalse(searchItems.isEmpty());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testFilterByStorage() {
        try {
            FiltersPage filtersPage = new FiltersPage(app.getDriver());
            filtersPage.checkStorage();

            Utils.sleep(2500);

            ResultsPage resultsPage = new ResultsPage(app.getDriver());
            List<SearchItem> searchItems = resultsPage.getItems();

            for (int i = 0; i < Math.min(searchItems.size(), 10); i++) {
                assertTrue(searchItems.get(i).isAvailable());
            }

        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/prametersForAuthors.csv", numLinesToSkip = 1)
    public void testFilterByAuthor(String author, String expectedTitle) {
        try {
            FiltersPage filtersPage = new FiltersPage(app.getDriver());
            filtersPage.searchByAuthor(author);

            Utils.sleep(2500);

            ResultsPage resultsPage = new ResultsPage(app.getDriver());
            List<SearchItem> searchItems = resultsPage.getItems();

            SearchItem firstItem = searchItems.get(0);
            assertEquals(expectedTitle, firstItem.getTitle());

        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testFilterByShipping() {
        try {
            FiltersPage filtersPage = new FiltersPage(app.getDriver());
            filtersPage.checkShipping();

            Utils.sleep(2500);

            ResultsPage resultsPage = new ResultsPage(app.getDriver());
            List<SearchItem> searchItems = resultsPage.getItems();

            assertFalse(searchItems.isEmpty());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSortingByPriceAsc() {
        try {
            FiltersPage filtersPage = new FiltersPage(app.getDriver());
            filtersPage.selectSorting(Sorting.PRICE_ASC);

            Utils.sleep(2500);

            ResultsPage resultsPage = new ResultsPage(app.getDriver());
            List<SearchItem> searchItems = resultsPage.getItems();

            for (int i = 1; i < Math.min(searchItems.size(), 10); i++) {
                assertTrue(searchItems.get(i - 1).getPrice() <= searchItems.get(i).getPrice());
            }
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSortingByAlphabetical() {
        try {
            FiltersPage filtersPage = new FiltersPage(app.getDriver());
            filtersPage.selectSorting(Sorting.ALPHABETICAL);

            Utils.sleep(2500);

            ResultsPage resultsPage = new ResultsPage(app.getDriver());
            List<SearchItem> searchItems = resultsPage.getItems();

            for (int i = 1; i < Math.min(searchItems.size(), 10); i++) {
                assertTrue(searchItems.get(i - 1).getTitle().toLowerCase().compareTo(searchItems.get(i).getTitle().toLowerCase()) <= 0);
            }
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @ParameterizedTest
    @CsvSource({
            "Láska ve střihu cosplaye",
            "Death Note"
    })
    public void testAddToCart(String searchTerm) {

        try {
            SearchPage searchPage = new SearchPage(app.getDriver());
            searchPage.search(searchTerm);

            Utils.sleep(2500);

            ResultsPage resultsPage = new ResultsPage(app.getDriver());
            List<SearchItem> results = resultsPage.getItems();
            assertFalse(results.isEmpty());

            SearchItem firstItem = results.get(0);

            resultsPage.addToCart(firstItem);
            Utils.sleep(2500);

            searchPage.clickBasketButton();
            Utils.sleep(2500);

            CartPage cartPage = new CartPage(app.getDriver());
            List<CartItem> cartItems = cartPage.getCartItems();

            assertTrue(cartItems.stream().anyMatch(e -> e.getTitle().equals(firstItem.getTitle())));

        } catch (ItemNotAvailable e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAddToCart_ItemNotAvailable() {
        try {
            SearchPage searchPage = new SearchPage(app.getDriver());
            searchPage.search("Batman");

            Utils.sleep(2500);

            ResultsPage resultsPage = new ResultsPage(app.getDriver());
            List<SearchItem> results = resultsPage.getItems();
            assertFalse(results.isEmpty());

            SearchItem firstItem = results.get(0);

            Exception exception = assertThrows(ItemNotAvailable.class, () -> resultsPage.addToCart(firstItem));

            String expectedMessage = firstItem.getTitle() + " is not available";
            String actualMessage = exception.getMessage();

            assertEquals(expectedMessage, actualMessage);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testRemoveCartItem() {
        try {
            SearchPage searchPage = new SearchPage(app.getDriver());
            searchPage.search("Death Note");

            Utils.sleep(2500);

            ResultsPage resultsPage = new ResultsPage(app.getDriver());
            List<SearchItem> results = resultsPage.getItems();
            assertFalse(results.isEmpty());

            SearchItem firstItem = results.get(0);

            resultsPage.addToCart(firstItem);
            Utils.sleep(2500);

            searchPage.clickBasketButton();
            Utils.sleep(2500);

            CartPage cartPage = new CartPage(app.getDriver());

            assertEquals(1, cartPage.getCartItems().size());

            cartPage.removeItem(cartPage.getCartItems().get(0));
            Utils.sleep(2500);

            cartPage.refresh();

            assertEquals(0, cartPage.getCartItems().size());

        } catch (ItemNotAvailable e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testLogin() {
        try {
            LoginPage loginPage = new LoginPage(app.getDriver());

            loginPage.login("tasafim823@crodity.com", "a1b2c3d4");
            Utils.sleep(7500);

            assertTrue(loginPage.isLogged());

        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testCheckoutProcessWithoutLogin() {
        try {

            SearchPage searchPage = new SearchPage(app.getDriver());
            searchPage.search("Láska ve střihu cosplaye");

            Utils.sleep(2500);

            ResultsPage resultsPage = new ResultsPage(app.getDriver());
            List<SearchItem> results = resultsPage.getItems();
            assertFalse(results.isEmpty());

            SearchItem firstItem = results.get(0);

            resultsPage.addToCart(firstItem);
            Utils.sleep(2500);

            searchPage.clickBasketButton();
            Utils.sleep(2500);

            CartPage cartPage = new CartPage(app.getDriver());
            cartPage.clickContinueButton();
            Utils.sleep(2500);

            ShippingAddressPage shippingAddressPage = new ShippingAddressPage(app.getDriver());

            shippingAddressPage.fillName("David");
            shippingAddressPage.fillSurname("Dolejší");
            shippingAddressPage.fillPhone("774153907");
            shippingAddressPage.fillEmail("dada542cz@gmail.com");
            shippingAddressPage.fillStreet("Okružní 542");
            shippingAddressPage.fillCity("Ledenice");
            shippingAddressPage.fillZip("37311");

            shippingAddressPage.clickNextButton();
            Utils.sleep(2500);

            PaymentPage paymentPage = new PaymentPage(app.getDriver());

            paymentPage.selectShippingMethod();
            Utils.sleep(2500);
            paymentPage.selectPaymentMethod();
            Utils.sleep(2500);

            paymentPage.clickNextButton();
            Utils.sleep(2500);

            WebDriver driver = app.getDriver();

            WebElement labelName = driver.findElement(By.className("address__label--name"));
            WebElement labelSurname = driver.findElement(By.className("address__label--surname"));
            WebElement labelStreet = driver.findElement(By.className("address__label--street"));
            WebElement labelZip = driver.findElement(By.className("address__label--zip"));
            WebElement labelCity = driver.findElement(By.className("address__label--city"));
            WebElement labelCountry = driver.findElement(By.className("address__label--country"));
            WebElement labelPhone = driver.findElement(By.className("address__label--phone"));
            WebElement labelShipping = driver.findElement(By.className("label--shipping"));
            WebElement labelPayment = driver.findElement(By.className("label--payment"));

            assertEquals("David", labelName.getText());
            assertEquals("Dolejší", labelSurname.getText());
            assertEquals("Okružní 542", labelStreet.getText());
            assertEquals("37311", labelZip.getText());
            assertEquals("Ledenice", labelCity.getText());
            //assertEquals("Česká republika", labelCountry.getText());
            //assertEquals("774153907", labelPhone.getText());
            assertTrue(labelShipping.getText().contains("Krakatit"));
            assertTrue(labelPayment.getText().contains("Krakatit"));

        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}